/* SECTIONS AND ORDER */

/* ----- go packages ----- */
/* ----- third party packages ----- */
/* ----- project packages ----- */

/* ----- types ----- */
/* ----- constants ----- */
/* ----- variables ----- */

/* ----- construct, destruct ----- */
/* ----- compare, observe ----- */
/* ----- copy, move, assign, modify ----- */
/* ----- compute, operate ----- */
/* ----- general routines ----- */
