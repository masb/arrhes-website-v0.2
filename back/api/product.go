package api

/* ----- go packages ----- */
import "fmt"
import "net/http"
import "strconv"
import "time"
/* ----- third party packages ----- */
import "pwf/src/attar"
import "pwf/src/enc"
/* ----- project packages ----- */
import "arrhes/back/db"
import dbm "arrhes/back/dbmodel"
import pl "arrhes/payload"

/* ----- types ----- */

type ProductHandler struct {
    Endpoint string `cbor:"product"`
    db.SQLHandle
}

type ProductCfgLoader struct {
    *ProductHandler `cbor:"endpoint_map"`
}

/* ----- constants ----- */

const singleTagSelectNameByPID =
    `SELECT t.name FROM ` + dbm.TagTable + ` t
        JOIN ` + dbm.TagRefTable + ` r ON r.tid = t.id
        WHERE t.meta = 0 AND r.pid = :id
    `

const allTagSelectNameByPID =
    `SELECT t.name FROM ` + dbm.TagTable + ` t
        JOIN ` + dbm.TagRefTable + ` r ON r.tid = t.id
        WHERE r.pid = :id
    `

/* ----- construct, destruct ----- */

func (this *ProductHandler) Init(cfg enc.CBOR) (attar.HandlerPath, error) {
    var err error

    ep := ProductCfgLoader{this}
    if err = cfg.Unmarshal(&ep); err != nil {
        goto abort
    }

    // if this.SQLHandle, err = db.NewSQLMemHandle(); err != nil {
    //     goto abort
    // }

    for _, i := range [...]string{
        dbm.SettingSchema, dbm.ProductSchema, dbm.TagSchema, dbm.TagRefSchema,
    } {
        if _, err = this.Exec(i); err != nil {
            goto abort
        }
    }

    if _, err = this.NamedInsert(dbm.ProductInsert, db.BindVar{
        "id": "12345", "name": "Product1",
    }); err != nil {
        goto abort
    }

    if _, err = this.NamedInsert(dbm.TagInsert, [...]dbm.Tag{
        {"67890", "Tag1", nil, false}, {"54321", "Category1", nil, true},
    }); err != nil {
        goto abort
    }

    if _, err = this.NamedInsert(dbm.TagRefInsert, [...]dbm.TagRef{
        {"12345", "67890"}, {"12345", "54321"},
    }); err != nil {
        goto abort
    }

    return attar.TrimedPath(this.Endpoint, true), nil
abort:
    return attar.NilHandlerPath(), fmt.Errorf("ProductHandler.Init [ERROR] %w", err)
}

/* ----- compute, operate ----- */

func (*ProductHandler) Setup(w http.ResponseWriter, r *http.Request) {
    attar.NotImplemented(w, "Setup")
}

func (this *ProductHandler) Dump(w http.ResponseWriter, r *http.Request) {
    var err error
    var ret []pl.Product
    var tagSelectNameByPID string
    var stmt db.SQLNamedStmt

    {
        var arg pl.ProductDump
        if err = enc.NewSourceCBOR(r.Body).Unmarshal(&arg); err != nil {
            goto abort
        }
        tagSelectNameByPID = singleTagSelectNameByPID
        if arg.CategoryMerge {
            tagSelectNameByPID = allTagSelectNameByPID
        }
    }

    if err = this.Select(&ret, dbm.ProductSelect); err != nil {
        goto abort
    }

    if stmt, err = this.PrepareNamed(tagSelectNameByPID); err != nil {
        goto abort
    }
    for i, n := 0, len(ret); i < n; i++ {
        if err = stmt.Select(&ret[i].TagList, ret[i]); err != nil {
            goto abort
        }
    }

    if err = enc.SinkCBOR(w, ret); err != nil {
        goto abort
    }

    return; abort:
    panic(fmt.Errorf("ProductHandler.Dump [ERROR] %w", err))
}

func (*ProductHandler) Drop(w http.ResponseWriter, r *http.Request) {
    attar.NotImplemented(w, "Drop")
}

func (this *ProductHandler) Create(w http.ResponseWriter, r *http.Request) {
    var err error

    ret := pl.Product{}
    if err = enc.NewSourceCBOR(r.Body).Unmarshal(&ret); err != nil {
        goto abort
    }

    ret.ID = strconv.FormatInt(time.Now().UnixNano(), 10)
    fmt.Printf("ProductHandler.Create [DEBUG] generating id %s for product %s\n", ret.ID, ret.Name)
    if _, err = this.NamedExec(dbm.ProductInsert, ret); err != nil {
        goto abort
    }
    for _, i := range ret.TagList {
        var tid string
        fmt.Printf("ProductHandler.Create [DEBUG] searching for tag %s\n", i)
        if err = this.GetIn(&tid, dbm.TagSelectIDByName, i); err != nil {
            goto abort
        }
        fmt.Printf("ProductHandler.Create [DEBUG] inserting tag (%s, %s) for product %s\n", tid, i, ret.Name)
        if _, err = this.Exec(dbm.TagRefInsertOrIgnore, ret.ID, tid); err != nil {
            goto abort
        }
    }

    if err = enc.SinkCBOR(w, ret); err != nil {
        goto abort
    }

    return; abort:
    panic(fmt.Errorf("ProductHandler.Create [ERROR] %w", err))
}

func (this *ProductHandler) Read(w http.ResponseWriter, r *http.Request) {
    var err error

    ret := pl.Product{}
    if err = enc.NewSourceCBOR(r.Body).Unmarshal(&ret); err != nil {
        goto abort
    }

    fmt.Printf("ProductHandler.Read [DEBUG] got id: %s", ret.ID)
    if err = this.NamedGet(&ret, dbm.ProductSelectByID, ret); err != nil {
        goto abort
    }

    if err = enc.SinkCBOR(w, ret); err != nil {
        goto abort
    }

    return
abort:
    panic(fmt.Errorf("ProductHandler.Read [ERROR] %w", err))
}

func (*ProductHandler) Update(w http.ResponseWriter, r *http.Request) {
    attar.NotImplemented(w, "Update")
}

func (*ProductHandler) Delete(w http.ResponseWriter, r *http.Request) {
    attar.NotImplemented(w, "Delete")
}
