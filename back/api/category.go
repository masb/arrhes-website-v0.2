package api

/* ----- go packages ----- */
import "fmt"
import "net/http"
/* ----- third party packages ----- */
import "pwf/src/attar"
import "pwf/src/enc"
/* ----- project packages ----- */
import "arrhes/back/db"
import dbm "arrhes/back/dbmodel"
import pl "arrhes/payload"

/* ----- types ----- */

type CategoryHandler struct {
    Endpoint string `cbor:"category"`
    db.SQLHandle
}

type CategoryCfgLoader struct {
    *CategoryHandler `cbor:"endpoint_map"`
}

/* ----- constants ----- */

const categorySelect = "SELECT id, name FROM " + dbm.TagTable + " WHERE meta = 1"

/* ----- construct, destruct ----- */

func (this *CategoryHandler) Init(cfg enc.CBOR) (attar.HandlerPath, error) {
    var err error

    ep := CategoryCfgLoader{this}
    if err = cfg.Unmarshal(&ep); err != nil {
        goto abort
    }

    // if this.SQLHandle, err = db.NewSQLMemHandle(); err != nil {
    //     goto abort
    // }

    for _, i := range [...]string{dbm.SettingSchema, dbm.TagSchema} {
        if _, err = this.Exec(i); err != nil {
            goto abort
        }
    }

    return attar.TrimedPath(this.Endpoint, true), nil
abort:
    return attar.NilHandlerPath(), fmt.Errorf("CategoryHandler.Init [ERROR] %w", err)
}

/* ----- compute, operate ----- */

func (*CategoryHandler) Setup(w http.ResponseWriter, r *http.Request) {
    attar.NotImplemented(w, "Setup")
}

func (this *CategoryHandler) Dump(w http.ResponseWriter, r *http.Request) {
    var err error

    ret := []pl.Category{}
    if err = this.Select(&ret, categorySelect); err != nil {
        goto abort
    }

    if err = enc.SinkCBOR(w, ret); err != nil {
        goto abort
    }

    return; abort:
    panic(fmt.Errorf("CategoryHandler.Dump [ERROR] %w", err))
}

func (*CategoryHandler) Drop(w http.ResponseWriter, r *http.Request) {
    attar.NotImplemented(w, "Drop")
}

func (this *CategoryHandler) Create(w http.ResponseWriter, r *http.Request) {
    attar.NotImplemented(w, "Create")
}

func (this *CategoryHandler) Read(w http.ResponseWriter, r *http.Request) {
    attar.NotImplemented(w, "Read")
}

func (*CategoryHandler) Update(w http.ResponseWriter, r *http.Request) {
    attar.NotImplemented(w, "Update")
}

func (*CategoryHandler) Delete(w http.ResponseWriter, r *http.Request) {
    attar.NotImplemented(w, "Delete")
}
