package api

/* ----- go packages ----- */
import "fmt"
import "net/http"
/* ----- third party packages ----- */
import "pwf/src/attar"
import "pwf/src/enc"
/* ----- project packages ----- */
import "arrhes/back/db"
import dbm "arrhes/back/dbmodel"
import pl "arrhes/payload"

/* ----- types ----- */

type OfferHandler struct {
    Endpoint string `cbor:"offer"`
    db.SQLHandle
}

type OfferCfgLoader struct {
    *OfferHandler `cbor:"endpoint_map"`
}

/* ----- construct, destruct ----- */

func (this *OfferHandler) Init(cfg enc.CBOR) (attar.HandlerPath, error) {
    var err error

    ep := OfferCfgLoader{this}
    if err = cfg.Unmarshal(&ep); err != nil {
        goto abort
    }

    // if this.SQLHandle, err = db.NewSQLMemHandle(); err != nil {
    //     goto abort
    // }

    for _, i := range [...]string{dbm.SettingSchema, dbm.OfferSchema, dbm.TagSchema} {
        if _, err = this.Exec(i); err != nil {
            goto abort
        }
    }

    if _, err = this.NamedInsert(dbm.OfferInsert, [...]pl.Offer{
        {"abc", "Offre de Janvier"},
        {"def", "Offre de Février"},
        {"ijk", "Offre de Mars"},
        {"lmn", "Offre d'Avril"},
    }); err != nil {
        goto abort
    }

    return attar.TrimedPath(this.Endpoint, true), nil
abort:
    return attar.NilHandlerPath(), fmt.Errorf("OfferHandler.Init: %w", err)
}

/* ----- compare, observe ----- */
/* ----- copy, move, assign, modify ----- */
/* ----- compute, operate ----- */

func (*OfferHandler) Setup(w http.ResponseWriter, r *http.Request) {
    attar.NotImplemented(w, "Setup")
}

func (*OfferHandler) Dump(w http.ResponseWriter, r *http.Request) {
    attar.NotImplemented(w, "Dump")
}

func (*OfferHandler) Drop(w http.ResponseWriter, r *http.Request) {
    attar.NotImplemented(w, "Drop")
}

func (*OfferHandler) Create(w http.ResponseWriter, r *http.Request) {
    attar.NotImplemented(w, "Create")
}

func (this *OfferHandler) Read(w http.ResponseWriter, r *http.Request) {
    var err error

    ret := pl.Offer{}
    if err = enc.NewSourceCBOR(r.Body).Unmarshal(&ret); err != nil {
        goto abort
    }

    println("OfferHandler.Read [DEBUG] received id:", ret.ID)

    if err = this.NamedGet(&ret, dbm.OfferSelectByID, ret); err != nil {
        goto abort
    }

    if err = enc.SinkCBOR(w, ret); err != nil {
        goto abort
    }

    return
abort:
    panic(fmt.Errorf("OfferHandler.Read: %w", err))
}

func (*OfferHandler) Update(w http.ResponseWriter, r *http.Request) {
    attar.NotImplemented(w, "Update")
}

func (*OfferHandler) Delete(w http.ResponseWriter, r *http.Request) {
    attar.NotImplemented(w, "Delete")
}

/* ----- general routines ----- */
