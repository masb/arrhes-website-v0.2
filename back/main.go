package main

/* ----- go packages ----- */
import "os"
/* ----- third party packages ----- */
import "pwf/src/attar"
/* ----- project packages ----- */
import "arrhes/back/api"
import "arrhes/back/db"

/* ----- */

func main() {
    var err error
    var h db.SQLHandle
    var handlerSet []attar.Handler
    var srv attar.Server

    if h, err = db.NewSQLMemHandle(); err != nil {
        goto abort
    }

    handlerSet = []attar.Handler{
        &AppHandler{}, &AdminHandler{}, &attar.StaticHandler{},
        attar.NewStoreHandler(&api.OfferHandler{SQLHandle: h}),
        attar.NewStoreHandler(&api.CategoryHandler{SQLHandle: h}),
        attar.NewStoreHandler(&api.ProductHandler{SQLHandle: h}),
    }

    if srv, err = attar.NewServer(os.Args, handlerSet...); err != nil {
        goto abort
    }

    if err = srv.ListenAndServe(); attar.ServerFailure(err) {
        goto abort
    }

    return; abort:
    panic(err)
}
