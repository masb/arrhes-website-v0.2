module Back extend Rake::DSL
    SRCDIR =        relpath __dir__

    GOSRC =         FileList["#{SRCDIR}/**/*.go"] +
                    FileList["src/**/*.go"]

    BIN =           outpath "#{NAME}"

    # ----- #

    task default: [:back]
    task clean: ['back:clean']

    task back: [BIN]
    file BIN => GOSRC do |t|
        sh "go build #{GO::DBGFLAGS} -o #{t.name} ./#{SRCDIR}"
    end

    namespace :back do
        task re: ['back:clean', BIN]
        task clean: [] do
            sh "rm -fv #{BIN}", verbose: false
        end

        task run: [BIN] do |t|
            sh "./#{t.source}"
        end
    end
end
