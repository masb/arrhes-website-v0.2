package main

/* ----- go packages ----- */
import "fmt"
/* ----- third party packages ----- */
import "pwf/src/attar"
import "pwf/src/enc"

/* ----- types ----- */

type AdminHandler struct {
    attar.WasmHandler
    Endpoint string `cbor:"admin"`
}

type AdminCfgLoader struct {
    Main *string `cbor:"admin_main"`
    *AdminHandler `cbor:"endpoint_map"`
}

/* ----- construct, destruct ----- */

func (this *AdminHandler) Init(cfg enc.CBOR) (attar.HandlerPath, error) {
    var err error

    ep := AdminCfgLoader{&this.Main, this}
    if err = cfg.Unmarshal(&ep); err != nil {
        goto abort
    }

    return attar.NewHandlerPath(this.Endpoint, true), nil
abort:
    return attar.NilHandlerPath(), fmt.Errorf("AdminHandler.Init: %w", err)
}
