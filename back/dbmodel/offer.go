package dbmodel

/* ----- types ----- */
/* ----- constants ----- */

const OfferTable = "offer"
const OfferSchema =
    `CREATE TABLE IF NOT EXISTS ` + OfferTable + ` (
        id TEXT NOT NULL UNIQUE,
        msg TEXT NOT NULL,
        PRIMARY KEY(id)
    )`

const OfferInsert =
    `INSERT INTO ` + OfferTable + ` (id, msg) VALUES (:id, :msg)`
const OfferSelectByID =
    `SELECT * FROM ` + OfferTable + ` WHERE id = :id`

/* ----- variables ----- */

/* ----- construct, destruct ----- */
/* ----- compare, observe ----- */
/* ----- copy, move, assign, modify ----- */
/* ----- compute, operate ----- */
/* ----- general routines ----- */
