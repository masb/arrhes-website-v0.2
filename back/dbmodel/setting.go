package dbmodel

/* ----- types ----- */
/* ----- constants ----- */

const SettingTable = "setting"
const SettingSchema =
    `CREATE TABLE IF NOT EXISTS ` + SettingTable + ` (
        key TEXT NOT NULL UNIQUE,
        value TEXT NOT NULL,
        PRIMARY KEY(key)
    )`

const SettingInsert =
    `INSERT INTO ` + SettingTable + ` (key, value) VALUES (:key, :value)`
const SettingSelectByID =
    `SELECT * FROM ` + SettingTable + ` WHERE key = :value`

/* ----- construct, destruct ----- */
/* ----- compare, observe ----- */
/* ----- copy, move, assign, modify ----- */
/* ----- compute, operate ----- */
/* ----- general routines ----- */
