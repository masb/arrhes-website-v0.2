package dbmodel

/* ----- types ----- */
/* ----- constants ----- */

const ProductTable = "product"
const ProductSchema =
    `CREATE TABLE IF NOT EXISTS ` + ProductTable + ` (
        id TEXT NOT NULL UNIQUE,
        name TEXT NOT NULL,
        PRIMARY KEY(id)
    )`

const ProductInsert =
    `INSERT INTO ` + ProductTable + ` (id, name) VALUES (:id, :name)`
const ProductSelect =
    `SELECT * FROM ` + ProductTable
const ProductSelectByID =
    `SELECT * FROM ` + ProductTable + ` WHERE id = :id`
const ProductSelectByName =
    `SELECT * FROM ` + ProductTable + ` WHERE name = :name`

/* ----- construct, destruct ----- */
/* ----- compare, observe ----- */
/* ----- copy, move, assign, modify ----- */
/* ----- compute, operate ----- */
/* ----- general routines ----- */
