package dbmodel

/* ----- types ----- */

type TagRef struct {
    PID, TID string
}

/* ----- constants ----- */

const TagRefTable = "tagref"
const TagRefSchema =
    `CREATE TABLE IF NOT EXISTS ` + TagRefTable + ` (
        pid TEXT NOT NULL,
        tid TEXT NOT NULL,
        UNIQUE (pid, tid)
    )`

const TagRefInsert =
    `INSERT INTO ` + TagRefTable + ` (pid, tid) VALUES (:pid, :tid)`
const TagRefInsertOrIgnore =
    `INSERT OR IGNORE INTO ` + TagRefTable + ` (pid, tid) VALUES (?, ?)`
const TagRefSelectByPID =
    `SELECT * FROM ` + TagRefTable + ` WHERE pid = :pid`
const TagRefSelectByTID =
    `SELECT * FROM ` + TagRefTable + ` WHERE tid = :tid`

/* ----- construct, destruct ----- */
/* ----- compare, observe ----- */
/* ----- copy, move, assign, modify ----- */
/* ----- compute, operate ----- */
/* ----- general routines ----- */
