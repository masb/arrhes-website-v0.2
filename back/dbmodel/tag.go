package dbmodel

/* ----- types ----- */

type Tag struct {
    ID, Name string
    Parent *string
    Meta bool
}

/* ----- constants ----- */

const TagTable = "tag"
const TagSchema =
    `CREATE TABLE IF NOT EXISTS ` + TagTable + ` (
        id TEXT NOT NULL UNIQUE,
        name TEXT NOT NULL UNIQUE,
        parent TEXT,
        meta INTEGER NOT NULL CHECK(meta IN(0, 1)) DEFAULT 0,
        PRIMARY KEY(id)
    )`

const TagInsert =
    `INSERT INTO ` + TagTable + `
        (id, name, parent, meta) VALUES (:id, :name, :parent, :meta)
    `

const TagSelectByID =
    `SELECT * FROM ` + TagTable + ` WHERE id = :id`
const TagSelectByName =
    `SELECT * FROM ` + TagTable + ` WHERE name = :name`
const TagSelectIDByName =
    `SELECT id FROM ` + TagTable + ` WHERE name = :name`

/* ----- construct, destruct ----- */
/* ----- compare, observe ----- */
/* ----- copy, move, assign, modify ----- */
/* ----- compute, operate ----- */
/* ----- general routines ----- */
