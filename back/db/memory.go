package db

/* ----- go packages ----- */
import "database/sql"
import "strings"
/* ----- third party packages ----- */
import _ "github.com/mattn/go-sqlite3"
import "github.com/jmoiron/sqlx"
/* ----- project packages ----- */

/* ----- types ----- */

type SQLHandle struct {
    *sqlx.DB
}

type SQLNamedStmt = *sqlx.NamedStmt

/* ----- constants ----- */

/* ----- construct, destruct ----- */

func NewSQLMemHandle() (SQLHandle, error) {
    db, err := sqlx.Open("sqlite3", ":memory:")
    return SQLHandle{db}, err
}

/* ----- compare, observe ----- */
/* ----- copy, move, assign, modify ----- */
/* ----- compute, operate ----- */

func (this SQLHandle) NamedInsert(q string, arg interface{}) (sql.Result, error) {
    return this.NamedExec(strings.TrimSpace(q), arg)
}

func (this SQLHandle) GetIn(ret interface{}, q string, arg interface{}) error {
    if q, argv, err := sqlx.In(q, arg); err != nil {
        return err
    } else {
        return this.Get(ret, q, argv...)
    }
}

func (this SQLHandle) NamedGet(ret interface{}, q string, arg interface{}) error {
    if stmt, err := this.PrepareNamed(q); err != nil {
        return err
    } else {
        return stmt.Get(ret, arg)
    }
}

func (this SQLHandle) NamedSelect(ret interface{}, q string, arg interface{}) error {
    if stmt, err := this.PrepareNamed(q); err != nil {
        return err
    } else {
        return stmt.Select(ret, arg)
    }
}

/* ----- general routines ----- */
