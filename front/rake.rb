module Front extend Rake::DSL, EnvWrapper
    SRCDIR =        relpath __dir__

    GOSRC =         FileList["#{SRCDIR}/**/*.go"] +
                    FileList["src/**/*.go"]

    BIN =           wwwpath "#{NAME}.wasm"

    GOOS =          "js"
    GOARCH =        "wasm"

    # ----- #

    task default: [:front]
    task clean: ['front:clean']

    task front: [BIN]
    file BIN => GOSRC do |t|
        sh self.as_env, "go build #{GO::FLAGS} -o #{t.name} ./#{SRCDIR}"
    end

    namespace :front do
        task re: ['front:clean', BIN]
        task clean: [] do
            sh "rm -fv #{BIN}", verbose: false
        end
    end
end
