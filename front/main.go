package main

/* third party packages */
import h "pwf/src/horon"
import "pwf/src/horon/svgtheme"
/* project packages */
import "arrhes/front/cmpt"
import um "arrhes/front/ssmodel/user"

/* ----- constants ----- */

const indexRoute = "/index"
const prestationRoute = "/offers"

/* ----- */

func main() {
    h.LoadTrusted(`<meta name="viewport" content="width=device-width, initial-scale=1">`)
    h.LoadCSS("/static/bulma.min.css")
    svgtheme.Load("/static/palette.svg")
    go um.Session.Load()

    app := h.App{RouteMap: map[string]h.Component{
        indexRoute:  &cmpt.Layout{Child: &cmpt.Index{}},
        prestationRoute:  &cmpt.Layout{Child: &cmpt.Prestation{}},
    }}
    app.Route(h.Body(), indexRoute)
    app.Wait()
}
