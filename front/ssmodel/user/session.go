package user

import h "pwf/src/horon"

/* ----- types ----- */

type session struct {
    DiscardOffer bool `cbor:"-"`
}

/* ----- constants ----- */

const sessionKey = "user_session"

/* ----- variables ----- */

var Session session

/* ----- construct, destruct ----- */

func (this *session) Load() {
    h.LocalStorageRead(this, sessionKey)
}

/* ----- copy, move, assign, modify ----- */

func (this *session) OnDiscardOffer() {
    this.DiscardOffer = true
    go this.Save()
}

/* ----- compute, operate ----- */

func (this *session) Save() {
    h.LocalStorageWrite(sessionKey, *this)
}
