package category

/* ----- third party packages ----- */
import h "pwf/src/horon"
/* ----- project packages ----- */
import pl "arrhes/payload"

/* ----- types ----- */

type list []pl.Category

/* ----- variables ----- */

var List list

/* ----- construct, destruct ----- */

func (this list) Load() {
    var err error
    h.ConsoleDebug("category.list.Load", "refreshing category list")
    req := h.RequestOpt{Method: "DUMP", URL: "/category"}
    if err = h.Request(&req, &List); err != nil {
        goto abort
    }
    return; abort:
    h.ConsoleError("category.list.Load", "%v", err)
}
