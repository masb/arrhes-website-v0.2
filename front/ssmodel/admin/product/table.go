package product

/* ----- third party packages ----- */
import h "pwf/src/horon"
/* ----- project packages ----- */
import pl "arrhes/payload"

/* ----- types ----- */

type pending pl.Product
type table []pl.Product

/* ----- constants ----- */

const tableKey = "admin_product_table"

/* ----- variables ----- */

var Pending pending
var Table table

/* ----- construct, destruct ----- */

func (this table) Load() {
    // h.LocalStorageRead(this, tableKey)
    var err error

    arg := pl.ProductDump{true}
    h.ConsoleDebug("product.table.Load", "refreshing product table")
    req := h.RequestOpt{Method: "DUMP", URL: "/product", Body: arg}
    if err = h.Request(&req, &Table); err != nil {
        goto abort
    }

    return
abort:
    h.ConsoleError("product.table.Load", "%v", err)
}

/* ----- copy, move, assign, modify ----- */

func (this *pending) OnNameInput(_ h.JSValue, argv []h.JSValue) interface{} {
    t := argv[0].Get("currentTarget")
    Pending.Name = t.Get("value").String()
    return nil
}

/* ----- compute, operate ----- */

// func (this table) Save() {
//     h.LocalStorageWrite(tableKey, *this)
// }
