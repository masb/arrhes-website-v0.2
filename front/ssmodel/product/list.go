package product

/* ----- third party packages ----- */
import h "pwf/src/horon"
/* ----- project packages ----- */
import pl "arrhes/payload"

/* ----- types ----- */

type list []pl.Product

/* ----- variables ----- */

var List list

/* ----- construct, destruct ----- */

func (this list) Load() {
    var err error
    h.ConsoleDebug("product.list.Load", "refreshing product list")
    req := h.RequestOpt{Method: "DUMP", URL: "/product"}
    if err = h.Request(&req, &List); err != nil {
        goto abort
    }
    return; abort:
    h.ConsoleError("product.list.Load", "%v", err)
}
