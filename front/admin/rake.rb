module Admin extend Rake::DSL, EnvWrapper
    SRCDIR =        relpath __dir__

    GOSRC =         FileList["#{SRCDIR}/**/*.go"] +
                    FileList["src/**/*.go"]

    BIN =           wwwpath "admin.wasm"

    GOOS =          "js"
    GOARCH =        "wasm"

    # ----- #

    task default: [:admin]
    task clean: ['admin:clean']

    task admin: [BIN]
    file BIN => GOSRC + Front::GOSRC do |t|
        sh self.as_env, "go build #{GO::FLAGS} -o #{t.name} ./#{SRCDIR}"
    end

    namespace :admin do
        task re: ['admin:clean', BIN]
        task clean: [] do
            sh "rm -fv #{BIN}", verbose: false
        end
    end
end
