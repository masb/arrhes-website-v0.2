package main

/* third party packages */
import h "pwf/src/horon"
import "pwf/src/horon/svgtheme"
/* project packages */
import "arrhes/front/cmpt/admin"

/* ----- constants ----- */

const indexRoute = "/index"
const productTableRoute = "/products"

/* ----- */

func main() {
    h.LoadTrusted(`<meta name="viewport" content="width=device-width, initial-scale=1">`)
    h.LoadCSS("/static/bulma.min.css")
    svgtheme.Load("/static/palette.svg")

    app := h.App{RouteMap: map[string]h.Component{
        indexRoute: &cmpt.Layout{Child: &cmpt.Index{}},
        productTableRoute: &cmpt.Layout{Child: &cmpt.ProductTable{}},
    }}
    app.Route(h.Body(), indexRoute)
    app.Wait()
}
