package cmpt

import h "pwf/src/horon"

/* ----- types ----- */

type Index struct {
    h.ComponentCache
}

/* ----- construct, destruct ----- */
/* ----- compare, observe ----- */
/* ----- copy, move, assign, modify ----- */
/* ----- compute, operate ----- */

func (this *Index) View() h.ElementView {
    return h.E("p", "Slt")
}
