package cmpt

/* third party packages */
import h "pwf/src/horon"
/* ----- project packages ----- */
import "arrhes/front/cmpt"
import pm "arrhes/front/ssmodel/admin/product"

/* ----- types ----- */

type ProductTable struct {
    h.ComponentCache
}

/* ----- constants ----- */

const productTableStyle = `
.tag input { border:none; box-shadow:none; }
.tag input:focus { outline:none; }
`

/* ----- construct, destruct ----- */

func (this *ProductTable) OnInit() {
    pm.Table.Load()
}

/* ----- compute, operate ----- */

func (*ProductTable) tbodyView() h.ElementView {
    var ret h.Children
    var tmp h.ElementView
    for _, i := range pm.Table {
        tmp = h.Child(&cmpt.Breadcrumb{LabelSet: i.TagList})
        tmp = h.E("tr", i.ID, i.Name, tmp).WrapEach("td")
        ret = append(ret, tmp)
    }
    return h.E("tbody", ret)
}

func (this *ProductTable) View() h.ElementView {
    return h.E("section", h.Children{
        h.E("style", productTableStyle),
        h.E("header", h.Class("navbar b_high"), h.Children{
            h.E("h1", h.Class("f_high"), "Products Table"),
        }.ClassWrap("navbar-item").ClassWrap("navbar-brand")),

        h.E("section", h.Class("column b_high"), h.Children{

            h.E("form", h.Class("box app-pending-product"), h.Children{
                h.E("p", h.Class("field"), h.Children{
                    h.E("ul", h.Class("control"), h.Children{
                        h.E("li", h.Class("control"), h.Children{
                            h.E("input", h.Class("input"), h.InputText(), h.Placeholder("Name"),
                                h.OnInput(pm.Pending.OnNameInput),
                                h.Attr("value", pm.Pending.Name),
                            ),
                        }),
                        h.E("li", h.Class("control"), h.Children{
                            h.E("button", h.Class("button"), "Insert"),
                        }),
                    }.ClassWrap("field is-grouped")),
                    h.E("ul", h.Class("help"), h.Children{
                        h.E("li", h.Class("control"), h.Children{
                            h.E("span", h.Class("tag"), "Jaj", h.E("button", h.Class("delete is-small"))),
                            h.E("span", h.Class("tag"), "Tag42", h.E("button", h.Class("delete is-small"))),
                            h.E("span", h.Class("tag b_high"), h.Children{
                                h.E("input", h.Placeholder("Tags (comma separated)")),
                            }),
                        }.ClassWrap("tags are-medium")),
                        h.E("li", h.Class("help"), h.Children{
                            h.E("li", h.E("a")),
                            h.E("li", h.E("a", "Bonjour")),
                            h.E("li", h.E("a", "Je suis")),
                            h.E("li", h.E("a", "L'autocomplete")),
                            h.E("li", h.E("a", "De tags")),
                        }.Wrap("ul").ClassWrap("breadcrumb is-small has-succeeds-separator")),
                    }.ClassWrap("field")),
                }),
            }),

            h.E("table", h.Class("table"), h.Children{
                h.E("thead", h.E("tr", "ID", "Name", "Tags").WrapEach("th")),
                this.tbodyView(),
            }),

        }),
    })
}
