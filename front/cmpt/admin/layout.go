package cmpt

import h "pwf/src/horon"

/* ----- types ----- */

type Layout struct {
    h.ComponentCache
    Route string
    Child h.Component
}

/* ----- constants ----- */

const layoutStyle = `
.app-menu h6 { color:var(--f_low); stroke:var(--f_low); }
.app-menu a, .app-menu a:hover { color:var(--f_low); stroke:var(--f_low); }
.app-menu a:hover { background:var(--b_low); }
.app-menu a.is-active { background:var(--b_low); color:var(--f_med); stroke:var(--f_med); }
.app-footer { background:var(--background); color:var(--f_low); stroke:var(--f_low); }
.app-footer a { color:var(--f_med); stroke:var(--f_med); }
`

/* ----- construct, destruct ----- */
/* ----- compare, observe ----- */
/* ----- copy, move, assign, modify ----- */
/* ----- compute, operate ----- */

func (this *Layout) View() h.ElementView {
    var a bool
    return h.E("fragment", h.Children{
        h.E("style", layoutStyle),
        h.E("section", h.Class("columns"), h.Children{
            h.E("aside", h.Class("column is-one-fifth"), h.Children{
                h.E("header", h.Class("navbar is-justify-content-flex-end bg"), h.Children{
                    h.E("figure", h.Class("navbar-brand"), h.Children{
                        h.E("a", h.Class("navbar-item"), h.Href("/"), h.Children{
                            h.E("img", h.Src("/static/logotype.png"), h.Attr("style", "height: 1em")),
                        }),
                    }),
                }),
                h.E("nav", h.Class("column menu app-menu"), h.Children{
                    h.E("h6", h.Class("menu-label"), "Overview"),
                    h.E("h6", h.Class("menu-label"), "Raw Data Tables"),
                    h.E("ul", h.Class("menu-list"), h.Children{
                        h.E("a", h.HrefIsHash(&a, "#"), h.ClassIf(a, "is-active"), "Users"),
                        h.E("a", h.HrefIsHash(&a, "#"), h.ClassIf(a, "is-active"), "Categories"),
                        h.E("a", h.HrefIsHash(&a, "#/products"), h.ClassIf(a, "is-active"), "Products"),
                        h.E("a", h.HrefIsHash(&a, "#"), h.ClassIf(a, "is-active"), "Offers"),
                    }.WrapEach("li")),
                }),
            }),
            h.E("main", h.Class("column"), h.Child(this.Child)),
        }),
        h.E("footer", h.Class("footer has-text-centered app-footer"), h.Children{
            h.E("p", "A website by ", h.E("a", h.Href("#"), "Paleă")),
            h.E("p", "Powered by ", h.E("a", h.Href("#"), "PWF")),
        }),
    })
}
