package cmpt

import h "pwf/src/horon"

/* ----- types ----- */

type Prestation struct {
    h.ComponentCache
}

/* ----- construct, destruct ----- */
/* ----- compare, observe ----- */
/* ----- copy, move, assign, modify ----- */
/* ----- compute, operate ----- */

func (this *Prestation) View() h.ElementView {
    return h.E("fragment", h.Children{
        h.E("section", h.Children{
            h.E("h1", "Location long terme"),
        }),
        h.E("section", h.Children{
            h.E("h1", "Offres d'affiliation"),
        }),
        h.E("section", h.Children{
            h.E("h1", "Offres de fidélité"),
        }),
    })
}
