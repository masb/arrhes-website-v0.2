package cmpt

/* third party packages */
import h "pwf/src/horon"
/* ----- project packages ----- */
import um "arrhes/front/ssmodel/user"
import pm "arrhes/front/ssmodel/product"
import cm "arrhes/front/ssmodel/category"
import pl "arrhes/payload"

/* ----- types ----- */

type Index struct {
    h.ComponentCache
    offerNotice OfferNotice
}

/* ----- constants ----- */

const indexStyle = `
.app-menu { background:var(--b_high); }
.app-menu h6 { color:var(--f_low); stroke:var(--f_low); }
.app-menu a, .app-menu a:hover { color:var(--f_low); stroke:var(--f_low); }
.app-menu a:hover { background:var(--b_low); }
.app-menu a.is-active { background:var(--b_low); color:var(--f_med); stroke:var(--f_med); }
`

/* ----- construct, destruct ----- */

func (this *Index) OnInit() {
    pm.List.Load()
    cm.List.Load()

    {
        var err error
        arg, ret := pl.NewProduct("Product2", []string{"Category1"}), pl.Product{}
        req := h.RequestOpt{Method: "CREATE", URL: "/product", Body: &arg}
        if err = h.Request(&req, &ret); err != nil {
            goto abort
        }
        h.ConsoleDebug("Index.OnInit", "%#v", ret)
        return; abort:
        h.ConsoleError("Index.OnInit", "%v", err)
    }
}

/* ----- compute, operate ----- */

func (this *Index) menuView() h.Children {
    var ret h.Children
    var tmp h.ElementView
    var a bool
    for _, i := range cm.List {
        tmp = h.E("a", h.HrefIsHash(&a, "#"), h.ClassIf(a, "is-active"), i.Name)
        ret = append(ret, tmp)
    }
    return ret.WrapEach("li")
}

func (this *Index) catalogView() h.Children {
    var ret h.Children
    for _, i := range pm.List {
        ret = append(ret, h.Child(&ProductCard{Product: &i}))
    }
    return ret.ClassWrapEach("column is-one-third")
}

func (this *Index) View() h.ElementView {
    return h.E("section", h.Class("container"), h.Children{
        h.E("style", indexStyle),
        h.E("aside", h.Class("column is-one-fifth"), h.Children{
            h.E("nav", h.Class("box menu app-menu"), h.Children{
                h.E("h6", h.Class("menu-label"), "Catégories"),
                h.E("ul", h.Class("menu-list"), this.menuView()),
            }),
        }),
        h.E("main", h.Class("column"), h.Children{
            h.ChildIf(!um.Session.DiscardOffer, &this.offerNotice),
            h.E("section", h.Class("columns"), this.catalogView()),
        }),
        h.E("aside", h.Class("column is-one-fifth"), h.Children{}),
    }.ClassWrap("columns").ClassWrap("column"))
}
