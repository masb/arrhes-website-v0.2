package cmpt

/* ----- go packages ----- */
import "strings"
/* ----- third party packages ----- */
import h "pwf/src/horon"

/* ----- types ----- */

type Breadcrumb struct {
    h.ComponentCache
    LabelSet, HrefSet []string
    IsSmall bool
}

/* ----- constants ----- */
/* ----- construct, destruct ----- */
/* ----- compute, operate ----- */

func (this *Breadcrumb) View() h.ElementView {
    var ret h.Children
    if len(this.HrefSet) == 0 {
        n := len(this.LabelSet)
        this.HrefSet = strings.SplitAfterN(strings.Repeat("#", n), "#", n)
    }
    for n, i := range this.LabelSet {
        ret = append(ret, h.E("a", h.Href(this.HrefSet[n]), i).Wrap("li"))
    }
    if !this.IsSmall {
        return h.E("ul", ret).ClassWrap("breadcrumb has-bullet-separator")
    }
    return h.E("ul", ret).ClassWrap("breadcrumb is-small has-bullet-separator")
}
