package cmpt

/* ----- third party packages ----- */
import h "pwf/src/horon"
/* ----- project packages ----- */
import pl "arrhes/payload"

/* ----- types ----- */

type ProductCard struct {
    h.ComponentCache
    *pl.Product
}

/* ----- constants ----- */
/* ----- construct, destruct ----- */
/* ----- compute, operate ----- */

func (this *ProductCard) View() h.ElementView {
    return h.E("article", h.Class("card app-card"), h.Children{
        h.E("figure", h.Class("image is-5by4"), h.Children{
            h.E("img", h.Src("https://bulma.io/images/placeholders/600x480.png")),
        }).ClassWrap("card-image"),
        h.E("section", h.Class("card-content"), h.Children{
            h.E("h6", h.Class("title is-6"), this.Name),
            h.ChildIf(len(this.TagList) != 0, &Breadcrumb{LabelSet: this.TagList, IsSmall: true}),
        }),
    })
}
