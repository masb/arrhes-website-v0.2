package cmpt

/* ----- third party packages ----- */
import h "pwf/src/horon"
/* ----- project packages ----- */
import um "arrhes/front/ssmodel/user"
import pl "arrhes/payload"

/* ----- types ----- */

type OfferNotice struct {
    h.ComponentCache
    pl.Offer
}

/* ----- constants ----- */

const offerNoticeStyle = `
.app-notice { background:var(--b_low); color:var(--f_med); stroke:var(--f_med); }
.app-notice .delete::before { background:var(--b_low); }
.app-notice .delete::after { background:var(--b_low); }
`

/* ----- construct, destruct ----- */

func (this *OfferNotice) OnInit() {
    var err error
    arg := pl.OfferID{ID: "def"}
    req := h.RequestOpt{Method: "READ", URL: "/offer", Body: &arg}
    if err = h.Request(&req, &this.Offer); err != nil {
        goto abort
    }
    return; abort:
    h.ConsoleError("OfferNotice.OnInit", "%v", err)
}

/* ----- compute, operate ----- */

func (this *OfferNotice) View() h.ElementView {
    return h.E("article", h.Class("notification app-notice"), h.Children{
        h.E("style", offerNoticeStyle),
        h.E("button", h.Class("delete"), h.OnClick(um.Session.OnDiscardOffer)),
        h.E("p", this.Message),
    })
}
