package cmpt

import h "pwf/src/horon"

/* ----- types ----- */

type Layout struct {
    h.ComponentCache
    Child h.Component
}

/* ----- constants ----- */

const layoutStyle = `
.app-navbar { background:var(--b_high); }
.app-navbar a { color:var(--f_med); stroke:var(--f_med); }
.app-navbar .is-rounded {
    border:none;
    background:var(--b_low);
    color:var(--f_med); stroke:var(--f_med);
}
.app-footer { background:var(--background); color:var(--f_low); stroke:var(--f_low); }
.app-footer a { color:var(--f_med); stroke:var(--f_med); }
`

/* ----- compute, operate ----- */

func (this *Layout) View() h.ElementView {
    return h.E("fragment", h.Children{
        h.E("style", layoutStyle),
        h.E("header", h.Class("navbar app-navbar"), h.Children{
            h.E("figure", h.Class("navbar-brand"), h.Children{
                h.E("a", h.Class("navbar-item"), h.Href("#/home"), h.Children{
                    h.E("img", h.Src("/static/logotype.png"), h.Attr("style", "height: 1em")),
                }),
            }),
            h.E("nav", h.Class("navbar-menu"), h.Children{
                h.E("section", h.Class("navbar-start"), h.Children{
                    h.E("form", h.Class("field has-addons"), h.Attr("autocomplete", "on"), h.Children{
                        h.E("input", h.Class("input"), h.InputText(), h.Placeholder("Rechercher un produit")),
                        h.E("a", h.Class("button"), h.E("i", h.Class("fa fa-search"))),
                    }.ClassWrapEach("control")).ClassWrap("navbar-item"),
                    h.E("a", h.Class("navbar-item"), h.Href("#"), "Qui sommes-nous ?"),
                    h.E("a", h.Class("navbar-item"), h.Href("#"), "FAQ"),
                    h.E("a", h.Class("navbar-item"), h.Href("#/offers"), "Offres"),
                }),
                h.E("section", h.Class("navbar-end"), h.Children{
                    h.E("a", h.Class("button is-rounded"), h.Href("#"), "Connexion"),
                }.ClassWrapEach("navbar-item")),
            }),
        }.ClassWrap("container")),
        h.Child(this.Child),
        h.E("footer", h.Class("footer has-text-centered app-footer"), h.Children{
            h.E("p", "A website by ", h.E("a", h.Href("#"), "Paleă")),
            h.E("p", "Powered by ", h.E("a", h.Href("#"), "PWF")),
        }),
    })
}
