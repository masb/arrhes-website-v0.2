module arrhes

go 1.14

replace pwf => ./pwf

require (
	github.com/jmoiron/sqlx v1.3.1
	github.com/mattn/go-sqlite3 v1.14.6
	golang.org/x/text v0.3.5 // indirect
	pwf v0.0.0-00010101000000-000000000000
)
