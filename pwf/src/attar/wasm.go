package attar

/* ----- go packages ----- */
import "fmt"
import "net/http"
import "path"
import "text/template"
/* ----- project packages ----- */
import "pwf/src/enc"

/* ----- types ----- */

type WasmHandler struct {
    Main string `cbor:"-"`
    Endpoint string `cbor:"wasm"`
    StaticEP string `cbor:"static"`
}

type WasmCfgLoader struct {
    Main *string `cbor:"wasm_main"`
    *WasmHandler `cbor:"endpoint_map"`
}

/* ----- construct, destruct ----- */

func (this *WasmHandler) Init(cfg enc.CBOR) (HandlerPath, error) {
    var err error

    ep := WasmCfgLoader{&this.Main, this}
    if err = cfg.Unmarshal(&ep); err != nil {
        goto abort
    }

    if this.Main == "" {
        this.Main = "main.wasm"
    }

    fmt.Printf("WasmHandler.Init [DEBUG] this: %#v\n", *this)
    return NewHandlerPath(this.Endpoint, true), nil
abort:
    return NilHandlerPath(), fmt.Errorf("WasmHandler.Init: %w", err)
}

/* ----- compute, operate ----- */

func (this *WasmHandler) Serve(w http.ResponseWriter, r *http.Request) {
    var err error

    // if r.ProtoMajor != 2 {
    //     w.WriteHeader(http.StatusInternalServerError)
    //     return
    // }

    ContentType(w, "application/xhtml+xml", true)

    t, lang := template.Template{}, w.Header().Get(ContentLanguageKey)
    if _, err = t.Parse(xhtml); err != nil {
        goto abort
    }

    if err = t.Execute(w, map[string]string{
        "main": this.Main, "lang": lang, "static": path.Join("/", lang, this.StaticEP),
    }); err != nil {
        goto abort
    }

    return
abort:
    panic(fmt.Errorf("WasmHandler) Serve: %w", err))
}

/* ----- */

const xhtml = `
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="{{.lang}}" xml:lang="{{.lang}}">
    <head>
        <link rel="shortcut icon" type="image/x-icon" href="{{.static}}/favicon.ico" />
        <title>Go WASM loader</title>
        <script src="{{.static}}/wasm_exec.js"></script>
        <script defer="defer">
            const go = new Go();
            WebAssembly.instantiateStreaming(fetch("{{.static}}/{{.main}}"), go.importObject).then((result) => {
                go.run(result.instance);
            });
        </script>
    </head>
    <body></body>
</html>
`
