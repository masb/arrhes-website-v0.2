package attar

/* ----- go packages ----- */
import "io"
import "net/http"
import "os"
import "strings"
/* ----- third party packages ----- */
/* ----- project packages ----- */
import "pwf/src/enc"

/* ----- types ----- */

type StaticHandler struct {
    http.Dir `cbor:"static_root"`
    Endpoint string `cbor:"static"`
}

type StaticCfgLoader struct {
    *StaticHandler `cbor:"endpoint_map"`
}

/* ----- constants ----- */

/* ----- construct, destruct ----- */

func (this *StaticHandler) Init(cfg enc.CBOR) (HandlerPath, error) {
    ep := StaticCfgLoader{this}
    err := cfg.Unmarshal(this)
    if err != nil {
        return NilHandlerPath(), err
    }
    err = cfg.Unmarshal(&ep)
    return PathAsRoot(this.Endpoint, false), err
}

/* ----- compare, observe ----- */
/* ----- copy, move, assign, modify ----- */
/* ----- compute, operate ----- */

func (this *StaticHandler) Serve(w http.ResponseWriter, r *http.Request) {
    r.URL.Path = strings.TrimPrefix(r.URL.Path, this.Endpoint)
    err := this.CatFile(w, r.URL.Path)
    if err != nil {
        if os.IsNotExist(err) {
            NotFound(w, r.URL.Path)
        } else {
            InternalError(w, err.Error())
        }
    }
}

func (this *StaticHandler) CatFile(w http.ResponseWriter, path string) error {
    f, err := this.Open(path)
    if err != nil {
        return err
    }

    stat, err := f.Stat()
    if err != nil {
        return err
    }

    if !stat.Mode().IsRegular() {
        return os.ErrNotExist
    }

    ContentTypeAuto(w, string(this.Dir)+path)
    return CatFile(w, f)
}

/* ----- general routines ----- */

func CatFile(w http.ResponseWriter, f http.File) error {
    _, err := io.Copy(w, f)
    return err
}
