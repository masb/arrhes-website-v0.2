package attar

/* ----- go packages ----- */
import "net/http"
/* ----- third party packages ----- */
/* ----- project packages ----- */
import "pwf/src/enc"

/* ----- types ----- */

type StoreHandler interface {
    Init(enc.CBOR) (HandlerPath, error)

    Setup(http.ResponseWriter, *http.Request)
    Dump(http.ResponseWriter, *http.Request)
    Drop(http.ResponseWriter, *http.Request)

    Create(http.ResponseWriter, *http.Request)
    Read(http.ResponseWriter, *http.Request)
    Update(http.ResponseWriter, *http.Request)
    Delete(http.ResponseWriter, *http.Request)
}

type StoreWrapper struct {
    StoreHandler
}

/* ----- constants ----- */

/* ----- construct, destruct ----- */
/* ----- compare, observe ----- */
/* ----- copy, move, assign, modify ----- */
/* ----- compute, operate ----- */

func (this StoreWrapper) Serve(w http.ResponseWriter, r *http.Request) {
    switch r.Method {
    case "SETUP": this.Setup(w, r)
    case "DUMP": this.Dump(w, r)
    case "DROP": this.Drop(w, r)
    case "CREATE": this.Create(w, r)
    case "READ": this.Read(w, r)
    case "UPDATE": this.Update(w, r)
    case "DELETE": this.Delete(w, r)
    default: InvalidMethod(w, r.Method)
    }
}

/* ----- general routines ----- */

func NewStoreHandler(h StoreHandler) StoreWrapper {
    return StoreWrapper{h}
}
