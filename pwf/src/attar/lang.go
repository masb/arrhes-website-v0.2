package attar

/* ----- go packages ----- */
import "golang.org/x/text/language"
import "net/http"
// import "path"
import "strings"
/* ----- third party packages ----- */
/* ----- project packages ----- */

/* ----- types ----- */

type LangHandler []string

/* ----- constants ----- */

const ContentLanguageKey = "Content-Language"

/* ----- construct, destruct ----- */

func (h LangHandler) Wrap(ret *http.Server) {
    sub, mux := ret.Handler, http.NewServeMux()
    mux.HandleFunc("/", h.ServeRoot)
    for _, i := range h {
        dup := "/" + i
        mux.HandleFunc(dup+"/", func(w http.ResponseWriter, r *http.Request) {
            r.URL.Path = strings.TrimPrefix(r.URL.Path, dup)
            w.Header().Set(ContentLanguageKey, dup[1:])
            sub.ServeHTTP(w, r)
        })
    }
    ret.Handler = mux
}

/* ----- compare, observe ----- */
/* ----- copy, move, assign, modify ----- */
/* ----- compute, operate ----- */

func (h LangHandler) ServeRoot(w http.ResponseWriter, r *http.Request) {
    tagMatcher := make([]language.Tag, len(h))
    for n, i := range h {
        tagMatcher[n] = language.Make(i)
    }
    matcher := language.NewMatcher(tagMatcher)
    cookie, _ := r.Cookie("lang")
    accept := r.Header.Get("Accept-Language")
    tag, _ := language.MatchStrings(matcher, cookie.String(), accept)
    lang, _ := tag.Base()
    url := *r.URL
    // if len(url.Path) <= 1 {
    //     url.Path = "/" + lang.String() + "/"
    // } else {
    //     url.Path = path.Join("/"+lang.String(), url.Path)
    // }
    url.Path = "/" + lang.String() + url.Path
    http.Redirect(w, r, url.String(), http.StatusTemporaryRedirect)
}

/* ----- general routines ----- */
