package attar

/* ----- */

import (
    "net/http"
    "path"
    "strings"

    "pwf/src/enc"
)

/* ----- */

type Server = *http.Server

/* ----- */

type Handler interface {
    Init(enc.CBOR) (HandlerPath, error)
    Serve(http.ResponseWriter, *http.Request)
}

type HandlerPath struct {
    Path string
    Fn   http.HandlerFunc

    Disabled, ExactMatch bool
    // Auth bool
}

type ServerCfgLoader struct {
    Addr string `cbor:"server_addr"`
    Lang LangHandler `cbor:"server_lang"`
}

/* ----- */

func NewServer(argv []string, handlerSet ...Handler) (Server, error) {
    cfgpath := path.Base(argv[0]) + enc.CfgExt
    if len(argv) > 1 {
        cfgpath = path.Join(argv[1], cfgpath)
    }

    cfg, err := enc.ParseFile(cfgpath)
    if err != nil {
        return nil, err
    }

    srv := ServerCfgLoader{}
    err = cfg.Unmarshal(&srv)
    if err != nil {
        return nil, err
    }
    println("NewServer [DEBUG] starting server at", srv.Addr, "- default lang:", srv.Lang[0])

    ret, mux := http.Server{Addr: srv.Addr}, http.NewServeMux()
    for _, i := range handlerSet {
        hp, err := i.Init(cfg)
        if err != nil {
            return nil, err
        }

        hp.Fn = i.Serve
        mux.HandleFunc(hp.Path, hp.Handle)
    }
    ret.Handler = mux

    if srv.Lang != nil {
        srv.Lang.Wrap(&ret)
    }
    return &ret, err
}

func ServerFailure(err error) bool {
    return err != nil && err != http.ErrServerClosed
}

/* ----- */

func (hp *HandlerPath) Handle(w http.ResponseWriter, r *http.Request) {
    println("HandlerPath.Handle [DEBUG] trace:", r.URL.Path)
    if !hp.Disabled {
        if hp.ExactMatch && r.URL.Path != hp.Path {
            http.NotFound(w, r)
            return
        }
        hp.Fn(w, r)
    }
}

func NewHandlerPath(p string, exactMatch bool) HandlerPath {
    return HandlerPath{Path: p, ExactMatch: exactMatch}
}

func NilHandlerPath() HandlerPath {
    return HandlerPath{}
}

func PathAsRoot(p string, exactMatch bool) HandlerPath {
    p = path.Join("/", p, "x")
    return NewHandlerPath(p[:len(p)-1], exactMatch)
}

func TrimedPath(p string, exactMatch bool) HandlerPath {
    return NewHandlerPath(strings.TrimSuffix(p, "/"), exactMatch)
}
