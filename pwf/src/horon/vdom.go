package horon

/* ----- go packages ----- */
import "strings"
/* ----- third party packages ----- */
/* ----- project packages ----- */

/* ----- types ----- */

type ElementView = *ElementBuilder
type Children []ElementView

/* ----- constants ----- */
/* ----- variables ----- */

/* ----- construct, destruct ----- */

func Txt(text string) ElementView {
    return (&ElementBuilder{Tag: "text", Text: text}).UpdateHash()
}

func TxtIf(cond bool, text string) ElementView {
    if cond {
        return Txt(text)
    }
    return nil
}

func TxtV(textv ...string) Children {
    ret := Children{}
    for _, i := range textv {
        ret = append(ret, Txt(i))
    }
    return ret
}

func E(tag string, argv ...interface{}) ElementView {
    ret := ElementBuilder{Tag: tag}
    for _, i := range argv {
        switch arg := i.(type) {
        case string:
            ret.Children = append(ret.Children, Txt(arg))
        case *AttributeBuilder:
            ret.AttrSet = append(ret.AttrSet, arg)
        case []*AttributeBuilder:
            ret.AttrSet = append(ret.AttrSet, arg...)
        case *FuncBuilder:
            ret.FnSet = append(ret.FnSet, arg)
        case ElementView:
            ret.Children = append(ret.Children, arg)
        case Children:
            ret.Children = append(ret.Children, arg...)
        }
    }
    return ret.UpdateHash()
}

func EIf(cond bool, tag string, argv ...interface{}) ElementView {
    if cond {
        return E(tag, argv...)
    }
    return nil
}

func ChildIf(cond bool, self Component) ElementView {
    if cond {
        return Child(self)
    }
    return nil
}

func Attr(k, v string) *AttributeBuilder {
    return &AttributeBuilder{Key: k, Value: v}
}

func AttrIf(cond bool, k, v string) *AttributeBuilder {
    if cond {
        return Attr(k, v)
    }
    return nil
}

func Class(classv ...string) []*AttributeBuilder {
    ret := make([]*AttributeBuilder, len(classv))
    for _, i := range classv {
        for _, j := range strings.Fields(i) {
            ret = append(ret, Attr("class", j))
        }
    }
    return ret
}

func ClassIf(cond bool, classv ...string) []*AttributeBuilder {
    if cond {
        return Class(classv...)
    }
    return nil
}

/* ----- compare, observe ----- */
/* ----- copy, move, assign, modify ----- */

func (this ElementView) Wrap(tag string) ElementView {
    return E(tag, this)
}

func (this ElementView) WrapInner(tag string) ElementView {
    this.Children = Children{E(tag, this.Children)}
    return this
}

func (this ElementView) WrapEach(tag string) ElementView {
    for n, i := range this.Children {
        this.Children[n] = E(tag, i)
    }
    return this
}

func (this Children) Wrap(tag string) ElementView {
    return E(tag, this)
}

func (this Children) WrapEach(tag string) Children {
    for n, i := range this {
        this[n] = E(tag, i)
    }
    return this
}

func (this ElementView) ClassWrap(class ...string) ElementView {
    return E("div", Class(class...), this)
}

func (this ElementView) ClassWrapInner(class ...string) ElementView {
    this.Children = Children{E("div", Class(class...), this.Children)}
    return this
}

func (this ElementView) ClassWrapEach(class ...string) ElementView {
    for n, i := range this.Children {
        this.Children[n] = E("div", Class(class...), i)
    }
    return this
}

func (this Children) ClassWrap(class ...string) ElementView {
    return E("div", Class(class...), this)
}

func (this Children) ClassWrapEach(class ...string) Children {
    for n, i := range this {
        this[n] = E("div", Class(class...), i)
    }
    return this
}

/* ----- compute, operate ----- */
/* ----- general routines ----- */

func Trust(html string) ElementView {
    return E("html", html)
}

func TrustIf(cond bool, html string) ElementView {
    if cond {
        return Trust(html)
    }
    return nil
}

func Value(text string) ElementView {
    return E("value", text)
}

func ValueIf(cond bool, text string) ElementView {
    if cond {
        return Value(text)
    }
    return nil
}

func Id(id string) *AttributeBuilder {
    return Attr("id", id)
}

func IdIf(cond bool, id string) *AttributeBuilder {
    if cond {
        return Id(id)
    }
    return nil
}

func Href(url string) *AttributeBuilder {
    return Attr("href", url)
}

func HrefIsHash(ret *bool, url string) *AttributeBuilder {
    *ret = url == LocationHash()
    return Href(url)
}

func Src(url string) *AttributeBuilder {
    return Attr("src", url)
}

func Input(typeStr string) *AttributeBuilder {
    return Attr("type", typeStr)
}

func InputText() *AttributeBuilder {
    return Input("text")
}

func Placeholder(text string) *AttributeBuilder {
    return Attr("placeholder", text)
}

/* ----- */

func OnClick(fn interface{}) *FuncBuilder {
    return &FuncBuilder{Key: "onclick", Fn: NewHook(fn)}
}

func OnInput(fn interface{}) *FuncBuilder {
    return &FuncBuilder{Key: "oninput", Fn: NewHook(fn)}
}

/* ----- */

func OnInit(fn interface{}) *FuncBuilder {
    return &FuncBuilder{Key: "oninit", Fn: NewHook(fn)}
}

func OnUpdate(fn interface{}) *FuncBuilder {
    return &FuncBuilder{Key: "onupdate", Fn: NewHook(fn)}
}
