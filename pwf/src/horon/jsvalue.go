package horon

/* ----- go packages ----- */
import "syscall/js"
/* ----- third party packages ----- */
import "pwf/src/enc"
/* ----- project packages ----- */

/* ----- types ----- */

type JSValue = js.Value

/* ----- constants ----- */
/* ----- variables ----- */

/* ----- construct, destruct ----- */

func Window() JSValue {
    return js.Global()
}

func Document() JSValue {
    return Window().Get("document")
}

func Body() JSValue {
    return Document().Get("body")
}

/* ----- compare, observe ----- */

func DocumentLang() string {
    return Document().Get("documentElement").Get("lang").String()
}

func LocationPath() string {
    return Window().Get("location").Get("pathname").String()
}

func LocationHash() string {
    return Window().Get("location").Get("hash").String()
}

func LocalStorageRead(ret interface{}, k string) {
    defer catchLocalStorageException()
    v := Window().Get("localStorage").Call("getItem", k)
    if !v.IsNull() {
        if err := enc.B64CBOR(v.String()).Unmarshal(ret); err != nil {
            ConsoleError("LocalStorageRead", err.Error())
        }
    }
}

/* ----- copy, move, assign, modify ----- */

func RemoveHashSneaky() {
    win := Window()
    l, h, d := win.Get("location"), win.Get("history"), win.Get("document")
    url := l.Get("pathname").String() + l.Get("search").String()
    h.Call("replaceState", h.Get("state"), d.Get("title"), url)
}

func PushLocationHash(hash string) {
    switch LocationHash() {
    case hash: RemoveHashSneaky()
    case "": Window().Get("location").Set("hash", hash)
    default: go Window().Call("onpopstate")
    }
}

func LocalStorageWrite(k string, v interface{}) {
    if ret, err := enc.ToCBOR(v); err != nil {
        ConsoleError("LocalStorageWrite", err.Error())
    } else {
        defer catchLocalStorageException()
        Window().Get("localStorage").Call("setItem", k, string(ret.B64()))
    }
}

/* ----- compute, operate ----- */
/* ----- general routines ----- */

func catchLocalStorageException() {
    if err := recover(); err != nil {
        switch err := err.(type) {
        case string: ConsoleError("catchLocalStorageException", err)
        case error: ConsoleError("catchLocalStorageException", err.Error())
        default: ConsoleError("catchLocalStorageException", "(%T) %v", err, err)
        }
    }
}
