package horon

import "sync"

/* ----- types ----- */

type App struct {
    fnMutex sync.Mutex
    fnCond *sync.Cond
    dispatchFn, exitFn Hook
    Root JSValue
    Cmpt Component
    DefaultRoute string
    RouteMap map[string]Component
}

/* ----- construct, destruct ----- */

func (this *App) Mount(root JSValue, c Component) {
    this.Root, this.Cmpt = root, c
    go this.AsyncMount()
}

func (this *App) Route(root JSValue, route string) {
    if this.RouteMap != nil {
        this.Root, this.DefaultRoute = root, "#" + route
        if !this.dispatchFn.JSFunc().IsUndefined() {
            this.dispatchFn.Free()
        }
        this.dispatchFn = WrapFunc(this.Dispatch)
        Window().Set("onpopstate", this.dispatchFn.JSFunc())
        PushLocationHash(this.DefaultRoute)
    }
}

func (this *App) Exit() {
    this.fnMutex.Lock()
    for _, i := range this.RouteMap {
        i.GetCache().VDOM.FreeAll()
    }
    this.dispatchFn.Free()
    this.exitFn.Free()
    ConsoleInfo("App.Exit", "app memory released")
    this.fnCond.Broadcast()
}

/* ----- compute, operate ----- */

func (this *App) Render() {
    ConsoleDebug("App.Render", "trace")
    this.fnMutex.Lock()
    self, hr := this.Cmpt, HookRegister{renderFn: this.Render}
    cache, vdom := self.GetCache(), self.View()
    if cache.VDOM == nil {
        ConsoleDebug("App.Render", "rendering DOM for %T", self)
        if self, ok := self.(ComponentWithInit); ok {
            hr.SpawnRedraw(self.OnInit)
        }
        cache.DOM = vdom.CreateDOM(&hr)
        for i, n := 0, len(hr.OnInit); i < n; i++ {
            hr.SpawnRedraw(hr.OnInit[i].Invoke)
        }
        this.Root.Set("innerHTML", "")
        this.Root.Call("appendChild", cache.DOM)
        cache.VDOM, cache.DOM = vdom, this.Root.Get("firstChild")
        if self, ok := self.(ComponentWithCreate); ok {
            hr.SpawnRedraw(self.OnCreate)
        }
    } else if cache.DOM.Equal(this.Root.Get("firstChild")) {
        if vdom.Hash != cache.VDOM.Hash {
            ConsoleDebug("App.Render", "updating DOM for %T", self)
            vdom.UpdateDOM(&hr, cache)
            for i, n := 0, len(hr.OnInit); i < n; i++ {
                hr.SpawnRedraw(hr.OnInit[i].Invoke)
            }
            self.GetCache().VDOM = vdom
            if self, ok := self.(ComponentWithUpdate); ok {
                hr.SpawnRedraw(self.OnUpdate)
            }
        }
    } else {
        ConsoleDebug("App.Render", "invalidating cache for %T", self)
        cache.VDOM.FreeAll()
        *cache = ComponentCache{}
        go this.Render()
    }
    for i, n := 0, len(hr.OnCreate); i < n; i++ {
        hr.SpawnRedraw(hr.OnCreate[i].Invoke)
    }
    for i, n := 0, len(hr.OnUpdate); i < n; i++ {
        hr.SpawnRedraw(hr.OnUpdate[i].Invoke)
    }
    this.fnMutex.Unlock()
}

func (this *App) AsyncMount() {
    // this.fnMutex.Lock()
    // this.fnMutex.Unlock()
    go this.Render()
}

func (this *App) Dispatch() {
    ConsoleDebug("App.Dispatch", "trace")
    this.fnMutex.Lock()
    hash := LocationHash()
    if hash == "" {
        hash = this.DefaultRoute
    } else if hash == this.DefaultRoute {
        RemoveHashSneaky()
    }

    var ok bool
    if this.Cmpt, ok = this.RouteMap[hash[1:]]; ok {
        ConsoleDebug("App.Dispatch", "LocationHash: %s", hash[1:])
        go this.AsyncMount()
    }
    this.fnMutex.Unlock()
}

func (this *App) Wait() {
    this.fnMutex.Lock()
    this.exitFn = WrapFunc(this.Exit)
    Window().Set("onbeforeunload", this.exitFn.JSFunc())
    this.fnCond = sync.NewCond(&this.fnMutex)
    this.fnCond.Wait()
}
