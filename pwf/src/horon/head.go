package horon

/* ----- general routines ----- */

func LoadTrusted(html string) {
    doc := Document()
    head := doc.Get("head")
    tmpl := doc.Call("createElement", "template")
    tmpl.Set("innerHTML", html)
    tmpl = tmpl.Get("content").Get("firstChild")
    head.Call("appendChild", tmpl)
}

func LoadCSS(uri string) {
    doc := Document()
    head := doc.Get("head")
    link := doc.Call("createElement", "link")
    link.Set("rel", "stylesheet")
    link.Set("href", uri)
    head.Call("appendChild", link)
}

func LoadStyle(css string) {
    doc := Document()
    head := doc.Get("head")
    style := doc.Call("createElement", "style")
    style.Set("innerHTML", css)
    head.Call("appendChild", style)
}

func LoadScript(uri string) {
    doc := Document()
    head := doc.Get("head")
    script := doc.Call("createElement", "script")
    script.Set("type", "text/javascript")
    script.Set("src", uri)
    script.Set("async", true)
    head.Call("appendChild", script)
}

func LoadCDATA(cdata string) {
    doc := Document()
    head := doc.Get("head")
    script := doc.Call("createElement", "script")
    script.Set("type", "text/javascript")
    script.Set("async", true)
    script.Set("innerHTML", "//<![CDATA[\n" + cdata + "\n//]]>")
    head.Call("appendChild", script)
}
