package horon

import "bytes"
import "context"
import "io"
import "net/http"
import "path"
import "strings"

import "pwf/src/enc"

/* ----- */

type RequestOpt struct {
    Method, URL string
    Param       map[string]interface{}
    Body        interface{}
}

/* ----- */

func CurrentPath() string {
    return strings.TrimPrefix(LocationPath(), "/" + DocumentLang())
}

func HostPath(url string) string {
    return path.Join("/", DocumentLang(), url)
}

func Request(opt *RequestOpt, ret interface{}) error {
    var err error
    var ctx = context.Background()
    var body io.Reader = nil

    if opt.Body != nil {
        cbor, err := enc.ToCBOR(opt.Body)
        if err != nil {
            return err
        }
        body = bytes.NewReader(cbor)
    }

    req, err := http.NewRequestWithContext(ctx, opt.Method, HostPath(opt.URL), body)
    if err != nil {
        return err
    }

    resp, err := http.DefaultClient.Do(req)
    if err != nil {
        return err
    }

    if resp.StatusCode == 200 {
        return enc.NewSourceCBOR(resp.Body).Unmarshal(ret)
    }
    return nil
}

// func RequestCb(opt *RequestOpt, fn func(Response, error)) error {
//     go func() {
//     }()
//     go func() {
//     }()
//     return nil
// }
