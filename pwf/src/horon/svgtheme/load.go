package svgtheme

/* ----- go packages ----- */
import "bytes"
import "fmt"
/* ----- project packages ----- */
import h "pwf/src/horon"

/* ----- types ----- */

type IFrame struct {
    h.JSValue
    Fn h.Hook
}

/* ----- constants ----- */

const Overrides = `
body, .bg { background:var(--background) !important; }
.f_high { color:var(--f_high) !important; stroke:var(--f_high) !important; }
.f_med { color:var(--f_med) !important; stroke:var(--f_med) !important; }
.f_low { color:var(--f_low) !important; stroke:var(--f_low) !important; }
.f_inv { color:var(--f_inv) !important; stroke:var(--f_inv) !important; }
.b_high { background:var(--b_high) !important; }
.b_med { background:var(--b_med) !important; }
.b_low { background:var(--b_low) !important; }
.b_inv { background:var(--b_inv) !important; }
`

/* ----- construct, destruct ----- */

func (this *IFrame) OnLoad() {
    svg := this.Get("contentDocument")
    keySet := [...]string{
        "background",
        "f_high", "f_med", "f_low", "f_inv",
        "b_high", "b_med", "b_low", "b_inv",
    }

    var v string
    buf := bytes.NewBufferString(Overrides + ":root{")
    for _, k := range keySet {
        v = svg.Call("getElementById", k).Call("getAttribute", "fill").String()
        fmt.Fprintf(buf, "--%s: %s;", k, v)
    }
    fmt.Fprint(buf, "}")

    h.LoadStyle(buf.String())
    this.Call("remove")
    this.Fn.Free()
}

/* ----- general routines ----- */

func Load(svg string) {
    doc := h.Document()
    head := doc.Get("head")
    iframe := IFrame{JSValue: doc.Call("createElement", "iframe")}
    iframe.Set("src", svg)
    iframe.Fn = h.WrapFunc(iframe.OnLoad)
    iframe.Call("addEventListener", "load", iframe.Fn.JSFunc())
    head.Call("appendChild", iframe.JSValue)
}
