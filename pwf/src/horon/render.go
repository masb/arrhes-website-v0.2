package horon

/* ----- types ----- */

type CmptHook struct {
    Fn Hook
    VDOM ElementView
    DOM JSValue
}

type HookRegister struct {
    OnInit []CmptHook
    OnCreate []CmptHook
    OnUpdate []CmptHook
    renderFn func()
}

type RedrawHook struct {
    Fn Hook
    OnRedraw func()
}

/* ----- construct, destruct ----- */

func NewVDOMHook(fn Hook, vdom *ElementBuilder) CmptHook {
    return CmptHook{Fn: fn, VDOM: vdom}
}

func NewDOMHook(fn Hook, vdom *ElementBuilder, dom JSValue) CmptHook {
    return CmptHook{Fn: fn, VDOM: vdom, DOM: dom}
}

func (this *HookRegister) AttachRedraw(fn *FuncBuilder) JSFunc {
    ret := RedrawHook{Fn: fn.Fn, OnRedraw: this.renderFn}
    return fn.Fn.StackUp(ret.Apply).Fn.(JSFunc)
}

/* ----- compute, operate ----- */

func (this *CmptHook) Invoke() interface{} {
    switch fn := this.Fn.Fn.(type) {
    case func(ElementView): fn(this.VDOM)
    case func(JSValue): fn(this.DOM)
    case func(ElementView, JSValue): fn(this.VDOM, this.DOM)
    default: return this.Fn.Apply(JSValue{}, []JSValue{this.DOM})
    }
    return nil
}

func (this *RedrawHook) Apply(self JSValue, argv []JSValue) interface{} {
    ret := this.Fn.Apply(self, argv)
    go this.OnRedraw()
    return ret
}

/* ----- general routines ----- */

func (this *HookRegister) SpawnRedraw(fn interface{}) {
    go func() {
        (&Hook{Fn: fn}).Invoke()
        go this.renderFn()
    }()
}

func Child(self Component) ElementView {
    ret, cache, vdom := E("child"), self.GetCache(), self.View()
    if cache.VDOM == nil {
        ConsoleDebug("RenderChild", "caching VDOM for %T", self)
        if self, ok := self.(ComponentWithInit); ok {
            ret.FnSet = append(ret.FnSet, OnInit(self.OnInit))
        }
        if self, ok := self.(ComponentWithCreate); ok {
            ret.FnSet = append(ret.FnSet, OnInit(self.OnCreate))
        }
        cache.VDOM = vdom
    } else if vdom.Hash != cache.VDOM.Hash {
        ConsoleDebug("RenderChild", "updating cached VDOM for %T", self)
        cache.VDOM = vdom
    } else {
        ConsoleDebug("RenderChild", "using cached VDOM for %T", self)
    }
    ret.Children = Children{cache.VDOM}
    return ret.UpdateHash()
}
