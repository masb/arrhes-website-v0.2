package horon

import "strings"

/* ----- */

func (this *ElementBuilder) CreateDOM(hr *HookRegister) (ret JSValue) {
    switch this.Tag {
    case "text", "#":
        return Document().Call("createTextNode", this.Text)
    case "html", "<":
        ret = Document().Call("createElement", "div")
        ret.Set("innerHTML", strings.TrimSpace(this.Text))
        return ret.Get("firstChild")
    case "child":
        this.createNode(hr, JSValue{})
        return this.Children[0].CreateDOM(hr)
    case "fragment", "[":
        ret = Document().Call("createDocumentFragment")
    default:
        ret = this.createNode(hr, this.createElement())
    }
    return this.createFragment(hr, ret)
}

func (this *ElementBuilder) createFragment(hr *HookRegister, ret JSValue) JSValue {
    for _, i := range this.Children {
        if i != nil {
            ret.Call("appendChild", i.CreateDOM(hr))
        }
    }
    return ret
}

func (this *ElementBuilder) createElement() JSValue {
    ret := Document().Call("createElement", this.Tag)
    for _, i := range this.AttrSet {
        if i != nil {
            switch i.Key {
            case "class":
                ret.Get("classList").Call("add", i.Value)
            default:
                ret.Call("setAttribute", i.Key, i.Value)
            }
        }
    }
    return ret
}

func (this *ElementBuilder) createNode(hr *HookRegister, ret JSValue) JSValue {
    funcMap := map[string]*FuncBuilder{}
    for _, i := range this.FnSet {
        if i != nil {
            switch i.Key {
            case "onupdate": break
            default: funcMap[i.Key] = i
            }
        }
    }
    if fn, ok := funcMap["oninit"]; ok {
        hr.OnInit = append(hr.OnInit, NewVDOMHook(fn.Fn, this))
        delete(funcMap, "oninit")
    }
    if fn, ok := funcMap["oncreate"]; ok {
        hr.OnCreate = append(hr.OnCreate, NewDOMHook(fn.Fn, this, ret))
        delete(funcMap, "oncreate")
    }
    for _, i := range funcMap {
        ret.Set(i.Key, hr.AttachRedraw(i))
    }
    return ret
}
