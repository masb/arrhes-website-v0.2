package horon

/* ----- go packages ----- */
import "fmt"

/* ----- general routines ----- */

func ConsoleDebug(id, f string, argv ...interface{}) {
    s := id + fmt.Sprintf(" [DEBUG] " + f, argv...)
    Window().Get("console").Call("debug", s)
}

func ConsoleInfo(id, f string, argv ...interface{}) {
    s := id + fmt.Sprintf(" [INFO] " + f, argv...)
    Window().Get("console").Call("info", s)
}

func ConsoleWarning(id, f string, argv ...interface{}) {
    s := id + fmt.Sprintf(" [WARNING] " + f, argv...)
    Window().Get("console").Call("warning", s)
}

func ConsoleError(id, f string, argv ...interface{}) {
    s := id + fmt.Sprintf(" [ERROR] " + f, argv...)
    Window().Get("console").Call("error", s)
}
