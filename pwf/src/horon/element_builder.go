package horon

import "bytes"
import "fmt"
import "hash/fnv"

/* ----- types ----- */

type AttributeBuilder struct {
    Key, Value string
}

type FuncBuilder struct {
    Key string
    Fn Hook
}

type ElementBuilder struct {
    Tag, Text string
    AttrSet []*AttributeBuilder
    FnSet []*FuncBuilder
    Children []*ElementBuilder
    Hash, ShallowHash uint32
}

/* ----- construct, destruct ----- */

func (this *ElementBuilder) FreeAll() {
    if this != nil {
        for _, i := range this.Children {
            i.FreeAll()
        }
        for _, i := range this.FnSet {
            i.Fn.Free()
        }
    }
}

/* ----- copy, move, assign, modify ----- */

func (this *ElementBuilder) UpdateHash() *ElementBuilder {
    var b bytes.Buffer
    fmt.Fprintf(&b, "%s %s\n", this.Tag, this.Text)
    for _, i := range this.AttrSet {
        if i != nil {
            fmt.Fprintf(&b, "%v ", *i)
        }
    }
    for _, i := range this.FnSet {
        if i != nil {
            fmt.Fprintf(&b, "%s %T", i.Key, i.Fn)
        }
    }
    this.ShallowHash = this.hashBytes(b.Bytes())
    for _, i := range this.Children {
        if i != nil {
            fmt.Fprintf(&b, "%x ", i.Hash)
        }
    }
    this.Hash = this.hashBytes(b.Bytes())
    return this
}

func (*ElementBuilder) hashBytes(b []byte) uint32 {
    ret := fnv.New32a()
    _, err := ret.Write(b)
    if err != nil {
        ConsoleError("ElementBuilder.UpdateHash", "%v", err)
    }
    return ret.Sum32()
}
