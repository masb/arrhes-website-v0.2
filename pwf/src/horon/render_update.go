package horon

import "strings"

/* ----- */

func (this *ElementBuilder) UpdateDOM(hr *HookRegister, cache *ComponentCache) JSValue {
    ret, updateMethod := JSValue{}, this.updateNode
    if this.Hash != cache.VDOM.Hash {
        switch this.Tag {
        case "text", "#":
            ConsoleDebug("ElementBuilder.UpdateDOM", "updating dirty text")
            ret = Document().Call("createTextNode", this.Text)
            return this.replaceNode(cache.DOM, ret)
        case "html", "<":
            ConsoleDebug("ElementBuilder.UpdateDOM", "updating dirty trusted html")
            ret = Document().Call("createElement", "div")
            ret.Set("innerHTML", strings.TrimSpace(this.Text))
            return this.replaceNode(cache.DOM, ret.Get("firstChild"))
        case "child":
            this.updateHooks(hr, &ComponentCache{VDOM: &ElementBuilder{}}, JSValue{})
            fallthrough
        case "fragment", "[":
            return this.updateFragment(hr, cache)
        case "input":
            updateMethod = this.updateInput
            fallthrough
        default:
            if this.ShallowHash != cache.VDOM.ShallowHash {
                ConsoleDebug("ElementBuilder.UpdateDOM", "updating dirty element %s", this.Tag)
                updateMethod(hr, cache)
            }
            if ret = cache.DOM.Get("firstChild"); ret.IsNull() {
                this.createFragment(hr, cache.DOM)
            } else {
                fw := ComponentCache{cache.VDOM, ret}
                this.removeNodeTail(cache.DOM, this.updateFragment(hr, &fw))
            }
        }
    } else if this.Tag == "fragment" || this.Tag == "[" {
        return this.skipFragment(cache)
    }
    return cache.DOM.Get("nextSibling")
}

func (this *ElementBuilder) updateFragment(hr *HookRegister, cache *ComponentCache) JSValue {
    var createMode bool
    var fw ComponentCache
    lct, ict := -1, 0
    next, parent := cache.DOM, JSValue{}
    for _, i := range this.Children {
        if i != nil {
            if createMode {
                ConsoleDebug("ElementBuilder.updateFragment", "appending newly created nodes to %s", parent.Get("tagName").String())
                parent.Call("appendChild", i.CreateDOM(hr))
            } else {
                lct = this.getChild(cache.VDOM, lct, ict); ict++
                if lct != -1 {
                    fw = ComponentCache{cache.VDOM.Children[lct], next}
                    if next = i.UpdateDOM(hr, &fw); !next.IsNull() {
                        continue
                    }
                }
                parent = cache.DOM.Get("parentNode")
                createMode = true
            }
        }
    }
    return next
}

func (this *ElementBuilder) skipFragment(cache *ComponentCache) JSValue {
    next := cache.DOM
    for _, i := range this.Children {
        if i != nil {
            next = next.Get("nextSibling")
        }
    }
    return next
}

func (this *ElementBuilder) updateHooks(hr *HookRegister, cache *ComponentCache, ret JSValue) {
    funcMap := map[string]*FuncBuilder{}
    for _, i := range cache.VDOM.FnSet {
        if i != nil {
            cache.DOM.Set(i.Key, JSValue{})
            i.Fn.Free()
        }
    }
    for _, i := range this.FnSet {
        if i != nil {
            switch i.Key {
            case "oninit", "oncreate": break
            default: funcMap[i.Key] = i
            }
        }
    }
    if fn, ok := funcMap["onupdate"]; ok {
        hr.OnUpdate = append(hr.OnUpdate, NewDOMHook(fn.Fn, this, ret))
        delete(funcMap, "onupdate")
    }
    for _, i := range funcMap {
        ret.Set(i.Key, hr.AttachRedraw(i))
    }
}

func (this *ElementBuilder) updateNode(hr *HookRegister, cache *ComponentCache) {
    ret := this.createElement()
    this.updateHooks(hr, cache, ret)

    ph := Document().Call("createElement", "div")
    ret.Call("appendChild", ph)

    ConsoleDebug("ElementBuilder.updateNode", "replacing node %s", this.Tag)
    parent := cache.DOM.Get("parentNode")
    parent.Call("insertBefore", ret, cache.DOM)
    ph.Get("replaceWith").Call("apply", ph, cache.DOM.Get("childNodes"))
    parent.Call("removeChild", cache.DOM)
    cache.DOM = ret
}

func (this *ElementBuilder) updateInput(hr *HookRegister, cache *ComponentCache) {
    attrSet := cache.DOM.Get("attributes")
    for attrSet.Length() > 0 {
        cache.DOM.Call("removeAttribute", attrSet.Index(0).Get("name"))
    }
    for _, i := range this.AttrSet {
        if i != nil {
            switch i.Key {
            case "class":
                cache.DOM.Get("classList").Call("add", i.Value)
            case "value":
                cache.DOM.Call("setAttribute", i.Key, i.Value)
                cache.DOM.Set("value", i.Value)
            default:
                cache.DOM.Call("setAttribute", i.Key, i.Value)
            }
        }
    }
    this.updateHooks(hr, cache, cache.DOM)
}

/* ----- general routines ----- */

func (*ElementBuilder) replaceNode(self, ret JSValue) JSValue {
    ConsoleDebug("ReplaceNode", "replacing node %s", self.Get("tagName").String())
    self.Call("replaceWith", ret)
    return ret.Get("nextSibling")
}

func (*ElementBuilder) getChild(self *ElementBuilder, lct, ict int) int {
    for i, n := lct + 1, len(self.Children); i < n; i++ {
        if self.Children[i] != nil {
            return i
        }
    }
    return -1
}

func (*ElementBuilder) removeNodeTail(self, tail JSValue) {
    var next JSValue
    for !tail.IsNull() {
        ConsoleDebug("ElementBuilder.removeNodeTail", "removing node %s", tail.Get("tagName").String())
        next = tail.Get("nextSibling")
        self.Call("removeChild", tail)
        tail = next
    }
}
