package horon

/* ----- types ----- */

type Component interface {
    View() ElementView
    GetCache() *ComponentCache
}

type ComponentWithInit interface {
    Component
    OnInit()
}

type ComponentWithCreate interface {
    Component
    OnCreate()
}

type ComponentWithUpdate interface {
    Component
    OnUpdate()
}

type ComponentCache struct {
    VDOM ElementView
    DOM JSValue
}

/* ----- copy, move, assign, modify ----- */

func (this *ComponentCache) GetCache() *ComponentCache {
    return this
}
