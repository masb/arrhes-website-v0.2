package horon

import "syscall/js"

/* ----- types ----- */

type JSFunc = js.Func
type BindTargetFunc = func(JSValue, []JSValue) interface{}

type Hook struct {
    Fn interface{}
    FreeFn func()
}

/* ----- variables ----- */

var fnAlloc = 0

/* ----- construct, destruct ----- */

func ToBindTarget(fn interface{}) BindTargetFunc {
    switch fn := fn.(type) {
    case func():
        return func(JSValue, []JSValue) interface{} { fn(); return nil }
    case func() interface{}:
        return func(JSValue, []JSValue) interface{} { return fn() }
    case BindTargetFunc:
        return fn
    }
    return nil
}

func ToJSFunc(fn interface{}) JSFunc {
    switch fn := fn.(type) {
    case func(), func() interface{}: return WrapFunc(fn).Fn.(JSFunc)
    case BindTargetFunc: return BindFunc(fn).Fn.(JSFunc)
    case JSFunc: return fn
    default: return JSFunc{}
    }
}

func NewHook(fn interface{}) Hook {
    return Hook{Fn: fn}
}

func BindFunc(fn BindTargetFunc) Hook {
    fnAlloc++
    ConsoleDebug("BindFunc", "allocated functions so far: %d", fnAlloc)
    return Hook{Fn: js.FuncOf(fn)}
}

func WrapFunc(fn interface{}) Hook {
    return BindFunc(ToBindTarget(fn))
}

func (this *Hook) Free() {
    if fn, ok := this.Fn.(JSFunc); ok && !fn.IsUndefined() {
        fnAlloc--
        fn.Release()
        ConsoleDebug("JSFunc.Free", "functions left to free: %d", fnAlloc)
    }
    if this.FreeFn != nil {
        this.FreeFn()
    }
}

/* ----- copy, move, assign, modify ----- */

func (this *Hook) Copy() Hook {
    return *this
}

func (this *Hook) StackUp(fn interface{}) Hook {
    ref := *this
    *this = NewHook(ToJSFunc(fn))
    this.FreeFn = ref.Free
    return *this
}

/* ----- compute, operate ----- */

func (this *Hook) JSFunc() JSFunc {
    return ToJSFunc(this.Fn)
}

func (this *Hook) Invoke() interface{} {
    switch fn := this.Fn.(type) {
    case func(): fn()
    case func() interface{}: return fn()
    case BindTargetFunc: return fn(JSValue{}, []JSValue{})
    case JSFunc: return fn.Invoke()
    }
    return nil
}

func (this *Hook) Apply(self JSValue, argv []JSValue) interface{} {
    switch fn := this.Fn.(type) {
    case func(): fn()
    case func() interface{}: return fn()
    case BindTargetFunc: return fn(self, argv)
    case JSFunc:
        jsargv := make([]interface{}, len(argv))
        for n, i := range argv {
            jsargv[n] = i
        }
        return fn.Call("apply", self, jsargv)
    }
    return nil
}
