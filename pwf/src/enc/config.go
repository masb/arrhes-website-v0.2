package enc

/* ----- go packages ----- */
import "encoding/json"
import "os"
/* ----- third party packages ----- */
/* ----- project packages ----- */

/* ----- types ----- */
/* ----- constants ----- */

const CfgExt = ".json"

/* ----- construct, destruct ----- */

func ParseFile(path string) (CBOR, error) {
    ret := map[string]interface{}{}
    f, err := os.Open(path)
    if err != nil {
        return nil, err
    }
    defer f.Close()
    err = json.NewDecoder(f).Decode(&ret)
    if err != nil {
        return nil, err
    }
    return ToCBOR(ret)
}

/* ----- compare, observe ----- */
/* ----- copy, move, assign, modify ----- */
/* ----- compute, operate ----- */
/* ----- general routines ----- */
