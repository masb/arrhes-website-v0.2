package payload

/* ----- types ----- */

type Category struct {
    ID, Name string
}
