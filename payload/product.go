package payload

/* ----- types ----- */

type ProductDump struct {
    CategoryMerge bool
}

type ProductID struct {
    ID string
}

type Product struct {
    ID, Name string
    TagList []string
}

/* ----- construct, destruct ----- */

func NewProduct(name string, tagList []string) Product {
    return Product{Name: name, TagList: tagList}
}
