package payload

/* ----- types ----- */

type OfferID struct {
    ID string
}

type Offer struct {
    ID string
    Message string `db:"msg" cbor:"msg"`
}
